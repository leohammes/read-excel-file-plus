import readXlsxFile from './source/read/readXlsxFileNode.js';

async function run() {
    const downloadedFilename = "spreadsheet.xlsx";
    const rows = await readXlsxFile(downloadedFilename)
    console.log(rows);
}

run();