"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = parseFileDrawings;

var _dom = require("../xml/dom.js");

var _parseFilePaths = _interopRequireDefault(require("./parseFilePaths.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _createForOfIteratorHelperLoose(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (it) return (it = it.call(o)).next.bind(it); if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; return function () { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/**
 * Returns sheet file drawings (images).
 * @param  {string} content — `xl/_rels/workbook.xml.rels` file contents.
 * @param  {object} xml
 * @return {object}
 */
function parseFileDrawings(drawingsContent, drawingsRelationsContent, xml) {
  var document = xml.createDocument(drawingsContent);
  var drawings = [];
  var anchors = (0, _dom.findChildren)(document.documentElement, "oneCellAnchor");
  var paths = (0, _parseFilePaths["default"])(drawingsRelationsContent, xml);

  for (var _iterator = _createForOfIteratorHelperLoose(anchors), _step; !(_step = _iterator()).done;) {
    var anchor = _step.value;
    var coordinates = (0, _dom.findChildren)(anchor, "from")[0];
    var picture = (0, _dom.findChildren)(anchor, "pic")[0];
    var col = (0, _dom.findChildren)(coordinates, "col")[0];
    var row = (0, _dom.findChildren)(coordinates, "row")[0];
    var blipFill = (0, _dom.findChildren)(picture, "blipFill")[0];
    var relation = (0, _dom.findChildren)(blipFill, "blip")[0].getAttribute('r:embed');
    var imagePath = paths.images[relation];
    drawings.push({
      col: col.textContent,
      row: row.textContent,
      imagePath: imagePath
    });
  }

  return drawings;
}
//# sourceMappingURL=parseFileDrawings.js.map