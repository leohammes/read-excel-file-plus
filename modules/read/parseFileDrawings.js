function _createForOfIteratorHelperLoose(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (it) return (it = it.call(o)).next.bind(it); if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; return function () { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

import { findChildren } from "../xml/dom.js";
import parseFilePaths from "./parseFilePaths.js";
/**
 * Returns sheet file drawings (images).
 * @param  {string} content — `xl/_rels/workbook.xml.rels` file contents.
 * @param  {object} xml
 * @return {object}
 */

export default function parseFileDrawings(drawingsContent, drawingsRelationsContent, xml) {
  var document = xml.createDocument(drawingsContent);
  var drawings = [];
  var anchors = findChildren(document.documentElement, "oneCellAnchor");
  var paths = parseFilePaths(drawingsRelationsContent, xml);

  for (var _iterator = _createForOfIteratorHelperLoose(anchors), _step; !(_step = _iterator()).done;) {
    var anchor = _step.value;
    var coordinates = findChildren(anchor, "from")[0];
    var picture = findChildren(anchor, "pic")[0];
    var col = findChildren(coordinates, "col")[0];
    var row = findChildren(coordinates, "row")[0];
    var blipFill = findChildren(picture, "blipFill")[0];
    var relation = findChildren(blipFill, "blip")[0].getAttribute('r:embed');
    var imagePath = paths.images[relation];
    drawings.push({
      col: col.textContent,
      row: row.textContent,
      imagePath: imagePath
    });
  }

  return drawings;
}
//# sourceMappingURL=parseFileDrawings.js.map