import { findChildren } from "../xml/dom.js";
import parseFilePaths from "./parseFilePaths.js";

/**
 * Returns sheet file drawings (images).
 * @param  {string} content — `xl/_rels/workbook.xml.rels` file contents.
 * @param  {object} xml
 * @return {object}
 */
export default function parseFileDrawings(drawingsContent, drawingsRelationsContent, xml) {
  const document = xml.createDocument(drawingsContent);

  const drawings = [];

  const anchors = findChildren(document.documentElement, "oneCellAnchor");

  const paths = parseFilePaths(drawingsRelationsContent, xml);

  for (let anchor of anchors) {
    const coordinates = findChildren(anchor, "from")[0];
    const picture = findChildren(anchor, "pic")[0];

    const col = findChildren(coordinates, "col")[0];
    const row = findChildren(coordinates, "row")[0];


    const blipFill = findChildren(picture, "blipFill")[0];
    const relation = findChildren(blipFill, "blip")[0].getAttribute('r:embed');

    const imagePath = paths.images[relation];

    drawings.push({
      col: col.textContent, row: row.textContent, imagePath
    });
  }

  return drawings;
}
